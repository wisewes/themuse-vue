# Components with Vue.js - TheMuse API

This a simple project using Vue.js to create components for displaying data from a REST call. The API is TheMuse /posts endpoint.

TheMuse does not require an app or client key or any authentication to hit their endpoints.

The API is searchable (in an annoying way) with a tag[] parameter. Many tags exists but many of them have no results associated with them.

This app doesn't use all of the tags, but only a handful structured into buttons.  The tag/categories chosen were picked because these categories represented a good data set when used. A few categories exhibit odd results from the API such as return a page_count of 2 but there only a few items in the results array resulting in only one page of results.
