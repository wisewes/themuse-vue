'use strict';


/**
 * basic app data
 */
var appData = {
  tags: [
    'Inspiration', 'Career', 'Communication', 'Coding', 'Entrepreneurship', 'Mentors', 'Networking', 'Tech'
  ]
};


/**
 * custom filter for formatting date
 */
Vue.filter('date', function(dt) {
  var formatted = new Date(dt);
  return (formatted.getMonth() + 1) + '/' + formatted.getDate() + '/' + formatted.getFullYear();
});


/**
 * buttons component
 */
Vue.component('buttons-comp', {

  props: {
    categories: {
      type: Array,
      required: true
    }
  }

});


/**
 * pager component
 */
Vue.component('pager', {

  props: {
    numOfPages: Number,
    currentPage: Number
  }
});


/**
 * muse-list component
 */
Vue.component('muse-list', {

  props: {
    listings: {
      type: Array,
      required: true
    }
  }
});


/**
 * main vue instance - mounted to body
 */
new Vue({

  el: 'body',

  data: {
    listings: [],
    categories: appData.tags,
    selectedCategory: '',
    currentPage: 0,
    pageCount: 0,
    numOfPages: 0,
    totalResults: 0,
    showList: false
  },

  ready: function() {

    console.log('vue instance ready');

    //init
    this.fetchMusePosts(1, 'Inspiration');
    $('.buttons-wrapper').find('button:first').addClass('active');
    $('body').find('.pagination > li:first').addClass('active');
  },

  methods: {

    fetchMusePosts: function(page, tag) {

      var self = this;
      var params = {
        descending: false
      };

      //check page param
      params.page = (page < 1) ? 1 : page;

      //check tag param
      if(tag === '') {
        params.tag = 'Inspiration';
      } else {
        params.tag = tag;
      }

      $.get('https://api-v1.themuse.com/posts?descending=' + params.descending + '&page=' + params.page + '&tag%5B%5D=' + params.tag)
      .fail(function(error) {
        console.log('get request failed ' + JSON.stringify(error));
      })
      .done(function(results) {

        //api will return error code and message in response
        if(results.hasOwnProperty('error')) {
            console.log('request error: ' + results.code + ' - ' + results.message);
            return;
        }

        self.listings = results.results;
        self.currentPage = results.page;

        //page_count is unreliable - tag of 'Design' will return a page_count of two with only 2 results; API is buggy
        self.pageCount = results.page_count;
        self.totalResults = self.pageCount * 20;

        self.numOfPages = Math.ceil(self.totalResults / 20) - 1;
        self.showList = true;
      });
    },

    changePage: function(event) {

      this.showList = false;

      var goToPage = parseInt(event.target.id.split('-')[1]);
      this.setActiveOnPager(event.target.id);


      if(typeof goToPage === 'number') {

        this.fetchMusePosts(goToPage, this.selectedCategory);
      } else {
        console.log('changePage - not a number');
      }
    },

    //unable to get v-bind class directive to work - hack to toggle active class
    setActiveOnPager: function(pageLink) {

      console.log('page link is ' + pageLink);

      $('.pagination').children().removeClass('active');
      $('#' + pageLink).parent().addClass('active');
    },

    setActiveOnButtons: function(button) {

      $('.buttons-wrapper').find('button').removeClass('active');
      $('#cat-' + button).addClass('active');
    },

    selectCategory: function(event) {

      var self = this;
      var cat = event.target.id.split('-')[1];
      self.selectedCategory = cat;

      this.fetchMusePosts(1, cat);
      this.setActiveOnButtons(cat);
    }
  }
});
